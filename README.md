# Création de notre outils interne de gestion de projet



## Contexte du projet

### Pour chaque projet, nous aurons besoin des informations suivantes :

## Un nom de projet

### Un description longue du projet
Cet outil nous permettra d'assigné des développeurs de l'agence sur chaque projet. Certains developpeur ne travaillerons que sur un seul projet, certains travaillerons sur plusieurs projets en même temps. Il est possible que certains developpeurs ne soient assigné à aucun projet.

Nous aurons aussi besoin de savoir pour à quel client appartient chaque projet.

Pour les développeurs de l'agence, nous devons connaitre leur nom, leur prénom et leur niveau (junior, sénior ou tech lead). Pour les clients, nous devons connaitre, leur nom et adresse.

Modalités pédagogiques
Dans un premier temps, vous devez réaliser le MCD (Modèle conceptuel de données) de la base de données de notre application
Ensuite, vous aller devoir créer cette base de données
Puis vous allez devoir commencer le développement de cette application.
Dans un premier temps, vous devez réaliser la partie Affichage / Ajout / Modification / Suppression des projets (sans tenir comptre des clients et des développeurs).

L'Application doit être conforme aux maquettes fournies. Elle doit être reponsive.

### Livrables
Un repo gitlab contenant : 
- Le MCD
- Un fichier SQL permettant de créer la base de données
- Le code source de la partie de l'application demandée (Affichage / Ajout / Modification / Suppression des projets) 
- Un fichier README expliquant commencer faire fonctionner l'application